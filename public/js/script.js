//Pace-master
Pace.on('done',function (e) {
    $('body').fadeIn();
});

$('.aplicaciones-checkbox').iCheck({
    checkboxClass: 'icheckbox_square-orange',
    radioClass: 'iradio_square-orange',
    increaseArea: '20%' // optional
});
//Plugin datatable
$('.datatable').DataTable({
    "language": {
        "sLengthMenu": "Mostrar _MENU_ registros",
        "search": "Buscar:",
        "infoEmpty": "No existen datos.",
        "sInfo": "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "paginate": {
            "next": "Siguiente",
            "previous": "Anterior"
        }
    }
});
//date picker
$.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Hoy",
    clear: "Limpiar",
    //format: "mm/dd/yyyy",
    //titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    //weekStart: 0
};
$('.fecha').datepicker({
    language: 'es',
    showOn: "button",
    allowInputToggle: true,
    pickerPosition: 'button-left',
    //format: "dd/mm/yyyy",
});
//Select2
$('.select2').select2();

//Funciones
$('#buscar-salones').click(function (e) {

    var data = {
        grupo:$('#grupo').val(),
        fecha:$('#fecha').val()
    };

    if (data.grupo != "" && data.fecha != ""){
        $.ajax({
            type: 'POST',
            url: $('#asignacion-manual-buscar-salon-form').attr('action'),
            data: data,
            dataType: 'json',
            success: function (e) {
                console.log(e);
                if (e.status){
                    $('#fecha-g').val(e.vista.grupo.fecha);
                    $('#fecha-t').html(e.vista.grupo.fecha);

                    $('#grupo-g').val(e.vista.grupo.GRU_CODIGO);
                    $('#grupo-t').html(e.vista.grupo.GRU_CODIGO);

                    $.each(e.vista.salones, function( i, index ) {
                        $('#salon-g').append('<option value="'+index.CODIGO+'" > '+index.CODIGO+' </option>');
                    });
                    //$("#salon-g option[value="+ e.vista.grupo.SAL_CODIGO +"]").attr("selected",true);
                    $("#salon-g").val(e.vista.grupo.SAL_CODIGO);

                    $('#asignacion-manual-modal').modal();
                }else{
                    swal({
                        type: 'warning',
                        title: 'Error!',
                        html: e.mensaje
                    });
                }
            },
            error: function (e) {
                swal({
                    type: 'error',
                    title: 'Error!',
                    html: 'Error de conexión'
                });
            }
        });
    }else{
        swal({
            type: 'warning',
            title: 'Alerta!',
            html: 'Complete los campos'
        });
    }
});

$('#guardar-salones').click(function (e) {


    $.ajax({
        type: 'POST',
        url: $('#asignacion-manual-guardar-salon-form').attr('action'),
        data: $('#asignacion-manual-guardar-salon-form').serialize(),
        dataType: 'json',
        success: function (e) {
            if (e.status){
                $("#asignacion-manual-modal").modal("hide");
                swal({
                    type: 'success',
                    title: '!Hecho',
                    html: e.mensaje
                });
            }else{
                swal({
                    type: 'error',
                    title: 'Error!',
                    html: e.mensaje
                });
            }
        },
        error: function (e) {
            swal({
                type: 'error',
                title: 'Error!',
                html: 'Error de conexión'
            });
        }
    });
});

$("#asignacion-manual-modal").on('hidden.bs.modal', function () {

    $('#salon-g option').each(function() {
        $(this).remove();
    });
    $('#salon-g').append('<option></option>');

    $('#fecha-t').html('');
    $('#grupo-t').html('');

    $('#asignacion-manual-guardar-salon-form')[0].reset();
});

$('#asignacion-automatica-form').submit(function (e) {
    e.preventDefault();
    Pace.start();
    $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: 'json',
        success: function (respuesta) {
            swal({
                type: respuesta.status,
                title: respuesta.title,
                html: respuesta.mensaje
            });
            Pace.stop();
        },
        error: function (e) {
            swal({
                type: 'error',
                title: 'Error!',
                html: 'Error de conexión'
            });
            Pace.stop();
        }
    });
});

var todayDate = moment().startOf('day');
var YM = todayDate.format('YYYY-MM');
var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
var TODAY = todayDate.format('YYYY-MM-DD');
var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

/*$('#calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
    },
    eventLimit: true, // allow "more" link when too many events
    navLinks: true
});*/

$('#asignacion-calendario-form').submit(function (e) {
    e.preventDefault();
    Pace.start();
    $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serialize(),
        //dataType: 'json',
        success: function (respuesta) {
            var rest = $.parseJSON(respuesta);
            if (rest.eventos != false){
                console.log(rest.eventos);
                var event = eval(rest.eventos);
                
                //$('#calendar').fullCalendar( 'renderEvent', event );
                $('#calendar').fullCalendar( 'destroy' );
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay,listWeek'
                    },
                    eventLimit: true, // allow "more" link when too many events
                    navLinks: true,
                    events:event
                });
            }
            swal({
                type: rest.status,
                title: rest.title,
                html: rest.mensaje
            });
            Pace.stop();
        },
        error: function (e) {
            swal({
                type: 'error',
                title: 'Error!',
                html: 'Error de conexión'
            });
            Pace.stop();
        }
    });
});