<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 6/04/18
 * Time: 11:19 AM
 * Author: Cesar Antolinez
 */
class MY_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $id_usuario = $this->session->userdata('id_usuario');
        //Validar si esta logeado
        if (empty($id_usuario) || $id_usuario == null){
            redirect('Login', 'location', 301);
        }
    }

    //Mensajes
    public function mensajes(){
        $mensaje = $this->session->userdata('mensaje');
        $envio = '';
        if ($mensaje['estado'] !== 0){
            $this->session->set_userdata(array('mensaje' => array('estado'=>0,'mensaje'=> '')));
            switch ($mensaje['estado']){
                case 0:
                    //No hay nada
                    $envio = '';
                    break;
                case 1:
                    //Mensaje afirmativo
                    $envio = $this->load->view('templates/mensajes/mensaje_afirmativo',$mensaje,true);
                    break;
                case 2:
                    //Mensaje Alerta
                    $envio = $this->load->view('templates/mensajes/mensaje_alerta',$mensaje,true);
                    break;
                case 3:
                    //Mensaje validación
                    $envio = $this->load->view('templates/mensajes/mensaje_validacion',$mensaje,true);
                    break;
                case 4:
                    //Mensaje Error
                    $envio = $this->load->view('templates/mensajes/mensaje_error',$mensaje,true);
                    break;
            }
        }
        return $envio;
    }



}

