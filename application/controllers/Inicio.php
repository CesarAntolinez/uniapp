<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 6/04/18
 * Time: 11:19 AM
 * Author: Cesar Antolinez
 */
class Inicio extends MY_Controller {


    public function index()
    {
        //Datos vista
        $title['title'] = 'Inicio';
        $footer['mensaje'] = $this->mensajes();

        //Vistas
        $this->load->view('templates/header',$title);
        $this->load->view('templates/menu');
        $this->load->view('inicio/inicio');
        $this->load->view('templates/footer',$footer);
    }
}
