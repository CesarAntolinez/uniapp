<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 22/04/2018
 * Time: 11:19 AM
 * Author: Cesar Antolinez
 */
class Usuarios extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        vista_aplicaciones(2);
    }

    public function index()
    {
        //Datos vista
        $title['title'] = 'Inicio';
        $footer['mensaje'] = $this->mensajes();
        $datos['usuarios'] = $this->usuarios->listar_usuarios();

        //Vistas
        $this->load->view('templates/header',$title);
        $this->load->view('templates/menu');
        $this->load->view('usuarios/usuarios_listar',$datos);
        $this->load->view('templates/footer',$footer);
    }

    //Crear el usuario
    public  function crear(){

        //Validar campos requeridos
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('apellido', 'Apellidos', 'required');
        $this->form_validation->set_rules('documento', 'Documento', 'required');
        $this->form_validation->set_rules('clave', 'Clave', 'required');
        //$this->form_validation->set_rules('aplicaciones[]', 'Aplicaciones', 'required');


        if ($this->form_validation->run() === FALSE) {
            //Datos para incluir en las vistas
            $titulo['titulo'] = 'Crear usuario';
            //$datos['id_usuario'] = $this->session->userdata('id_usuario');
            $datos['aplicaiones'] = $this->aplicaciones->listar_aplicaciones();
            $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion',array(),true): '';

            //Llamar la vista
            $this->load->view('templates/header',$titulo);
            $this->load->view('templates/menu');
            $this->load->view('usuarios/usuario_crear',$datos);
            $this->load->view('templates/footer');
        } else {
            //Organizar los datos para crear el cliente
            $envio['NOMBRE'] = $this->input->post('nombre');
            $envio['APELLIDO'] = $this->input->post('apellido');
            $envio['DOCUMENTO'] = $this->input->post('documento');
            $envio['CLAVE'] = md5($this->input->post('clave'));

            //Crear usuarios
            $id_usuario = $this->usuarios->crear_usuario($envio);
            //Validar si se creo
            if ($id_usuario != false){

                //Igreso las aplicaiones
                $aplicaiones = $this->input->post('aplicaciones');
                if (!empty($aplicaiones)){
                    foreach ($aplicaiones as $item){
                        $envio_aplicaiones []= array(
                            'COD_APLICACION'=>$item,
                            'COD_USUARIO'=>$id_usuario
                        );
                    }
                    $id_aplicaciones = $this->apli_usuarios->crear_apli_usuario($envio_aplicaiones);
                    if ($id_aplicaciones != false){
                        //Creación exitosa
                        $this->session->set_userdata(array('mensaje' => array('estado'=>1,'mensaje'=> 'Creación exitosa del usuario '.$id_usuario)));
                    }else{
                        //Creación exitosa
                        $this->session->set_userdata(array('mensaje' => array('estado'=>2,'mensaje'=> 'Creación exitosa del usuario '.$id_usuario.'<br>Los accesos no se crearon')));
                    }
                }else{
                    //Creación exitosa
                    $this->session->set_userdata(array('mensaje' => array('estado'=>1,'mensaje'=> 'Creación exitosa del usuario '.$id_usuario)));
                }
            }else{
                //Creación fallida
                $this->session->set_userdata(array('mensaje' => array('estado'=>4,'mensaje'=> 'No se pudo crear el cliente')));
            }
            redirect('Usuarios');
        }
    }

    //Modificar el usuario
    public  function modificar($id_usuario)
    {

        //Validar que el id exista
        if (!empty($id_usuario)) {
            //Llamar datos del cliente
            $datos['usuario'] = $this->usuarios->ver_usuario($id_usuario);

            if (!empty($datos['usuario']) && $datos['usuario'] != null) {
                //Validar campos requeridos
                $this->form_validation->set_rules('nombre', 'Nombre', 'required');
                $this->form_validation->set_rules('apellido', 'Apellidos', 'required');
                $this->form_validation->set_rules('documento', 'Documento', 'required');
                $this->form_validation->set_rules('clave', 'Clave', 'required');
                //$this->form_validation->set_rules('aplicaciones[]', 'Aplicaciones', 'required');


                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Modificar usuario';
                    //$datos['id_usuario'] = $this->session->userdata('id_usuario');
                    $datos['aplicaiones'] = $this->aplicaciones->listar_aplicaciones();
                    $datos['apli_usuario'] = $this->apli_usuarios->ver_apli_usuario($id_usuario);
                    $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion', array(), true) : '';

                    //Llamar la vista
                    $this->load->view('templates/header', $titulo);
                    $this->load->view('templates/menu');
                    $this->load->view('usuarios/usuario_modificar', $datos);
                    $this->load->view('templates/footer');
                } else {
                    //Organizar los datos para crear el cliente
                    $envio['NOMBRE'] = $this->input->post('nombre');
                    $envio['APELLIDO'] = $this->input->post('apellido');
                    $envio['DOCUMENTO'] = $this->input->post('documento');
                    $envio['CLAVE'] = md5($this->input->post('clave'));

                    //Crear usuarios
                    $id_usuario_e = $this->usuarios->modificar_usuario($id_usuario, $envio);
                    //Validar si se creo
                    if ($id_usuario_e != false) {

                        //Igreso las aplicaiones
                        $acesos = $this->apli_usuarios->eliminar_apli_usuario($id_usuario);
                        if ($acesos != false){
                            $aplicaiones = $this->input->post('aplicaciones');
                            if (!empty($aplicaiones)) {
                                foreach ($aplicaiones as $item) {
                                    $envio_aplicaiones [] = array(
                                        'COD_APLICACION' => $item,
                                        'COD_USUARIO' => $id_usuario
                                    );
                                }
                                $id_aplicaciones = $this->apli_usuarios->crear_apli_usuario($envio_aplicaiones);
                                //$id_aplicaciones = true;
                                if ($id_aplicaciones != false) {
                                    //Creación exitosa
                                    $this->session->set_userdata(array('mensaje' => array('estado' => 1, 'mensaje' => 'Modificación exitosa del usuario ' . $id_usuario)));
                                } else {
                                    //Creación exitosa pero api_usuarios se elimino
                                    $this->session->set_userdata(array('mensaje' => array('estado' => 2, 'mensaje' => 'Modificación exitosa del usuario ' . $id_usuario . '<br>Los accesos se eliminaron')));
                                }
                            } else {
                                //Creación exitosa pero api_usuarios no
                                $this->session->set_userdata(array('mensaje' => array('estado' => 1, 'mensaje' => 'Modificación exitosa del usuario ' . $id_usuario)));
                            }
                        }else{
                            //Creación exitosa pero api_usuarios no
                            $this->session->set_userdata(array('mensaje' => array('estado' => 2, 'mensaje' => 'Modificación exitosa del usuario ' . $id_usuario . '<br>Los accesos no se modificaron')));
                        }
                    } else {
                        //Creación fallida
                        $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se pudo modificar el usuario ')));
                    }
                    redirect('Usuarios');
                }

            } else {
                //No se encontro el cliente
                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el usuario')));
                redirect('Usuarios');
            }
        } else {
            //No se encontro el cliente
            $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el usuario')));
            redirect('Usuarios');
        }
        //redirect('Usuarios');

    }

    //Eliminar el usuario
    public function eliminar($id_usuario)
    {
        //Validar que el id exista
        if (!empty($id_usuario)) {
            //Llamar datos del cliente
            $datos['usuario'] = $this->usuarios->ver_usuario($id_usuario);

            if (!empty($datos['usuario']) && $datos['usuario'] != null) {
                //Validar campos requeridos
                $this->form_validation->set_rules('eliminar', 'Eliminar', 'required');

                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Eliminar usuario';
                    $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion', array(), true) : '';

                    //Llamar la vista
                    $this->load->view('templates/header', $titulo);
                    $this->load->view('templates/menu');
                    $this->load->view('usuarios/usuario_eliminar', $datos);
                    $this->load->view('templates/footer');
                } else {
                    //Organizar los datos para crear el cliente
                    $eliminar = $this->input->post('eliminar');
                    if ($eliminar === 'ELIMINAR'){
                        $id_usuario_e = $this->usuarios->eliminar_usuario($id_usuario);

                        if ($id_usuario_e != false){
                            //Se elimino correctamente
                            $this->session->set_userdata(array('mensaje' => array('estado' => 1, 'mensaje' => 'Eliminación exitosa del usuario ' . $id_usuario)));
                        }else{
                            //Eliminación fallida
                            $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se pudo eliminar el usuario '.$id_usuario)));
                        }
                    }else{
                        //Se escribio mal "ELIMINAR"
                        $this->session->set_userdata(array('mensaje' => array('estado' => 2, 'mensaje' => 'No se pudo eliminar el usuario ' . $id_usuario . '<br>Escribio mal "ELIMINAR"')));
                    }
                    redirect('Usuarios');
                }
            } else {
                //No se encontro el cliente
                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el usuario')));
                redirect('Usuarios');
            }
        } else {
            //No se encontro el cliente
            $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el usuario')));
            redirect('Usuarios');
        }
    }
}