<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 29/04/2018
 * Time: 12:11 PM
 */
class Administrar extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /** Programas */

    //Ver programas
    public function programas()
    {
        //Restriccion
        vista_aplicaciones(3);

        //Datos vista
        $title['title'] = 'Programas';
        $footer['mensaje'] = $this->mensajes();
        $datos['programas'] = $this->programas->listar_programas();

        //Vistas
        $this->load->view('templates/header',$title);
        $this->load->view('templates/menu');
        $this->load->view('administrar/programas/programas_listar',$datos);
        $this->load->view('templates/footer',$footer);
    }

    //Crear el programa
    public  function crear_programa(){

        //Restriccion
        vista_aplicaciones(3);

        //Validar campos requeridos
        $this->form_validation->set_rules('codigo', 'Código', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');

        if ($this->form_validation->run() === FALSE) {
            //Datos para incluir en las vistas
            $titulo['titulo'] = 'Crear programa';
            $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion',array(),true): '';

            //Llamar la vista
            $this->load->view('templates/header',$titulo);
            $this->load->view('templates/menu');
            $this->load->view('administrar/programas/programa_crear');
            $this->load->view('templates/footer');
        } else {
            //Organizar los datos para crear el programa
            $envio['CODIGO'] = $this->input->post('codigo');
            $envio['NOMBRE'] = $this->input->post('nombre');

            //Validar codigo
            if ($this->programas->ver_programa($envio['CODIGO']) != null){
                $titulo['titulo'] = 'Crear programa';
                $titulo['mensaje'] = $this->load->view('templates/mensajes/mensaje_alerta',array('mensaje' => 'El código ya existe'),true);

                //Llamar la vista
                $this->load->view('templates/header',$titulo);
                $this->load->view('templates/menu');
                $this->load->view('administrar/programas/programa_crear');
                $this->load->view('templates/footer');
            }else{
                //Crear programa
                $id_programa = $this->programas->crear_programa($envio);
                //Validar si se creo
                if ($id_programa != false){
                    //Creación exitosa
                    $this->session->set_userdata(array('mensaje' => array('estado'=>1,'mensaje'=> 'Creación exitosa del programa '.$id_programa)));
                }else{
                    //Creación fallida
                    $this->session->set_userdata(array('mensaje' => array('estado'=>4,'mensaje'=> 'No se pudo crear el programa ')));
                }
                redirect('Administrar/programas');
            }
        }
    }

    //modificar el programa
    public  function modificar_programa($id_programa){

        //Restriccion
        vista_aplicaciones(3);

        //Validar que el id exista
        if (!empty($id_programa)) {
            //Llamar datos del cliente
            $datos['programa'] = $this->programas->ver_programa($id_programa);

            if (!empty($datos['programa']) && $datos['programa'] != null) {

                //Validar campos requeridos
                $this->form_validation->set_rules('codigo', 'Código', 'required');
                $this->form_validation->set_rules('nombre', 'Nombre', 'required');

                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Modificar programa';
                    $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion',array(),true): '';

                    //Llamar la vista
                    $this->load->view('templates/header',$titulo);
                    $this->load->view('templates/menu');
                    $this->load->view('administrar/programas/programa_modificar',$datos);
                    $this->load->view('templates/footer');
                } else {
                    //Organizar los datos para crear el programa
                    $envio['CODIGO'] = $this->input->post('codigo');
                    $envio['NOMBRE'] = $this->input->post('nombre');
                    $codigo = $envio['CODIGO'];
                    $validar = $this->programas->ver_programa($envio['CODIGO']);

                    //Validar codigo
                    if ($validar != null && $codigo != $id_programa){
                        $titulo['titulo'] = 'Modificar programa';
                        $titulo['mensaje'] = $this->load->view('templates/mensajes/mensaje_alerta',array('mensaje' => 'El código ya existe'),true);

                        //Llamar la vista
                        $this->load->view('templates/header',$titulo);
                        $this->load->view('templates/menu');
                        $this->load->view('administrar/programas/programa_modificar',$datos);
                        $this->load->view('templates/footer');
                    }else{
                        //Crear programa
                        $id_programa_e = $this->programas->modificar_programa($id_programa, $envio);
                        //Validar si se creo
                        if ($id_programa_e != false){
                            //Creación exitosa
                            $this->session->set_userdata(array('mensaje' => array('estado'=>1,'mensaje'=> 'Modificación exitosa del programa '.$id_programa)));
                        }else{
                            //Creación fallida
                            $this->session->set_userdata(array('mensaje' => array('estado'=>4,'mensaje'=> 'No se pudo modificar el programa '.$id_programa)));
                        }
                        redirect('Administrar/programas');
                    }
                }
            } else {
                //No se encontro el programa
                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el programa')));
                redirect('Administrar/programas');
            }
        } else {
            //No se encontro el programa
            $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el programa')));
            redirect('Administrar/programas');
        }
    }

    //Eliminar el usuario
    public function eliminar_programa($id_programa)
    {

        //Restriccion
        vista_aplicaciones(3);

        //Validar que el id exista
        if (!empty($id_programa)) {
            //Llamar datos del cliente
            $datos['programa'] = $this->programas->ver_programa($id_programa);

            if (!empty($datos['programa']) && $datos['programa'] != null) {
                //Validar campos requeridos
                $this->form_validation->set_rules('eliminar', 'Eliminar', 'required');

                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Eliminar programa';
                    $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion', array(), true) : '';

                    //Llamar la vista
                    $this->load->view('templates/header', $titulo);
                    $this->load->view('templates/menu');
                    $this->load->view('/administrar/programas/programa_eliminar', $datos);
                    $this->load->view('templates/footer');
                } else {
                    //Organizar los datos para crear el cliente
                    $eliminar = $this->input->post('eliminar');
                    if ($eliminar === 'ELIMINAR'){

                        if ($this->grupos->validar_grupo($id_programa) == null){
                            $id_programa_e = $this->programas->eliminar_programa($id_programa);

                            if ($id_programa_e != false){
                                //Se elimino correctamente
                                $this->session->set_userdata(array('mensaje' => array('estado' => 1, 'mensaje' => 'Eliminación exitosa del programa ' . $id_programa)));
                            }else{
                                //Eliminación fallida
                                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se pudo eliminar el programa '.$id_programa)));
                            }
                        }else{
                            //Si hay grupos anclados al programa no deja eliminar
                            $this->session->set_userdata(array('mensaje' => array('estado' => 2, 'mensaje' => 'No se pudo eliminar el programa ' . $id_programa . '<br>El programa pertenece a uno o mas grupos')));
                        }
                    }else{
                        //Se escribio mal "ELIMINAR"
                        $this->session->set_userdata(array('mensaje' => array('estado' => 2, 'mensaje' => 'No se pudo eliminar el programa ' . $id_programa . '<br>Escribio mal "ELIMINAR"')));
                    }
                    redirect('Administrar/programas');
                    //redirect('Inicio');
                }
            } else {
                //No se encontro el cliente
                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el programa')));
                redirect('Administrar/programas');
            }
        } else {
            //No se encontro el cliente
            $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el programa')));
            redirect('Administrar/programas');
        }
    }

    /** Fin Programas */

    /** Edificios */

    //Ver programas
    public function edificios()
    {

        //Restriccion
        vista_aplicaciones(4);

        //Datos vista
        $title['title'] = 'Edificios';
        $footer['mensaje'] = $this->mensajes();
        $datos['edificios'] = $this->edificios->listar_edificios();

        //Vistas
        $this->load->view('templates/header',$title);
        $this->load->view('templates/menu');
        $this->load->view('administrar/edificios/edificios_listar',$datos);
        $this->load->view('templates/footer',$footer);
    }

    //Crear el programa
    public  function crear_edificio(){


        //Restriccion
        vista_aplicaciones(4);

        //Validar campos requeridos
        $this->form_validation->set_rules('codigo', 'Código', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('estado', 'Estado', 'required');

        if ($this->form_validation->run() === FALSE) {
            //Datos para incluir en las vistas
            $titulo['titulo'] = 'Crear edificio';
            $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion',array(),true): '';

            //Llamar la vista
            $this->load->view('templates/header',$titulo);
            $this->load->view('templates/menu');
            $this->load->view('administrar/edificios/edificios_crear');
            $this->load->view('templates/footer');
        } else {
            //Organizar los datos para crear el programa
            $envio['CODIGO'] = $this->input->post('codigo');
            $envio['NOMBRE'] = $this->input->post('nombre');
            $envio['ESTADO'] = $this->input->post('estado');

            //Validar codigo
            if ($this->edificios->ver_edificio($envio['CODIGO']) != null){
                $titulo['titulo'] = 'Crear edificio';
                $titulo['mensaje'] = $this->load->view('templates/mensajes/mensaje_alerta',array('mensaje' => 'El código ya existe'),true);

                //Llamar la vista
                $this->load->view('templates/header',$titulo);
                $this->load->view('templates/menu');
                $this->load->view('administrar/edificios/edificios_crear');
                $this->load->view('templates/footer');
            }else{
                //Crear programa
                $id_programa = $this->edificios->crear_edificio($envio);
                //Validar si se creo
                if ($id_programa != false){
                    //Creación exitosa
                    $this->session->set_userdata(array('mensaje' => array('estado'=>1,'mensaje'=> 'Creación exitosa del edificio '.$id_programa)));
                }else{
                    //Creación fallida
                    $this->session->set_userdata(array('mensaje' => array('estado'=>4,'mensaje'=> 'No se pudo crear el edificio ')));
                }
                redirect('Administrar/edificios');
            }
        }
    }

    //modificar el programa
    public  function modificar_edificio($id_edificio){


        //Restriccion
        vista_aplicaciones(4);

        //Validar que el id exista
        if (!empty($id_edificio)) {
            //Llamar datos del cliente
            $datos['edificio'] = $this->edificios->ver_edificio($id_edificio);

            if (!empty($datos['edificio']) && $datos['edificio'] != null) {

                //Validar campos requeridos
                $this->form_validation->set_rules('codigo', 'Código', 'required');
                $this->form_validation->set_rules('nombre', 'Nombre', 'required');
                $this->form_validation->set_rules('estado', 'Estado', 'required');


                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Modificar edificio';
                    $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion',array(),true): '';

                    //Llamar la vista
                    $this->load->view('templates/header',$titulo);
                    $this->load->view('templates/menu');
                    $this->load->view('administrar/edificios/edificios_modificar',$datos);
                    $this->load->view('templates/footer');
                } else {
                    //Organizar los datos para crear el programa
                    $envio['CODIGO'] = $this->input->post('codigo');
                    $envio['NOMBRE'] = $this->input->post('nombre');
                    $envio['ESTADO'] = $this->input->post('estado');

                    $codigo = $envio['CODIGO'];
                    $validar = $this->edificios->ver_edificio($envio['CODIGO']);

                    //Validar codigo
                    if ($validar != null && $codigo != $id_edificio){
                        $titulo['titulo'] = 'Modificar edificio';
                        $titulo['mensaje'] = $this->load->view('templates/mensajes/mensaje_alerta',array('mensaje' => 'El código ya existe'),true);

                        //Llamar la vista
                        $this->load->view('templates/header',$titulo);
                        $this->load->view('templates/menu');
                        $this->load->view('administrar/edificios/edificios_modificar',$datos);
                        $this->load->view('templates/footer');
                    }else{
                        //Crear programa
                        $id_edificio_e = $this->edificios->modificar_edificio($id_edificio, $envio);
                        //Validar si se creo
                        if ($id_edificio_e != false){
                            //Creación exitosa
                            $this->session->set_userdata(array('mensaje' => array('estado'=>1,'mensaje'=> 'Modificación exitosa del edificio '.$id_edificio)));
                        }else{
                            //Creación fallida
                            $this->session->set_userdata(array('mensaje' => array('estado'=>4,'mensaje'=> 'No se pudo modificar el edificio '.$id_edificio)));
                        }
                        redirect('Administrar/edificios');
                    }
                }
            } else {
                //No se encontro el programa
                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el edificio')));
                redirect('Administrar/edificios');
            }
        } else {
            //No se encontro el programa
            $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el edificio')));
            redirect('Administrar/edificios');
        }
    }

    //Eliminar el usuario
    public function eliminar_edificio($id_edificio)
    {

        //Restriccion
        vista_aplicaciones(4);

        //Validar que el id exista
        if (!empty($id_edificio)) {
            //Llamar datos del cliente
            $datos['edificio'] = $this->edificios->ver_edificio($id_edificio);

            if (!empty($datos['edificio']) && $datos['edificio'] != null) {
                //Validar campos requeridos
                $this->form_validation->set_rules('eliminar', 'Eliminar', 'required');

                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Eliminar edificio';
                    $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion', array(), true) : '';

                    //Llamar la vista
                    $this->load->view('templates/header', $titulo);
                    $this->load->view('templates/menu');
                    $this->load->view('administrar/edificios/edificios_eliminar', $datos);
                    $this->load->view('templates/footer');
                } else {
                    //Organizar los datos para crear el cliente
                    $eliminar = $this->input->post('eliminar');
                    if ($eliminar === 'ELIMINAR'){

                        if ($this->salones->validar_edificio($id_edificio) == null){
                            $id_programa_e = $this->edificios->eliminar_edificio($id_edificio);

                            if ($id_programa_e != false){
                                //Se elimino correctamente
                                $this->session->set_userdata(array('mensaje' => array('estado' => 1, 'mensaje' => 'Eliminación exitosa del edificio ' . $id_edificio)));
                            }else{
                                //Eliminación fallida
                                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se pudo eliminar el edificio '.$id_edificio)));
                            }
                        }else{
                            //Si hay salones anclados al edificio no deja eliminar
                            $this->session->set_userdata(array('mensaje' => array('estado' => 2, 'mensaje' => 'No se pudo eliminar el edificio ' . $id_edificio . '<br>El edificio pertenece a uno o mas salones')));
                        }
                    }else{
                        //Se escribio mal "ELIMINAR"
                        $this->session->set_userdata(array('mensaje' => array('estado' => 2, 'mensaje' => 'No se pudo eliminar el edificio ' . $id_edificio . '<br>Escribio mal "ELIMINAR"')));
                    }
                    redirect('Administrar/edificios');
                    //redirect('Inicio');
                }
            } else {
                //No se encontro el edificio
                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el edificio')));
                redirect('Administrar/edificios');
            }
        } else {
            //No se encontro el edificio
            $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el edificio')));
            redirect('Administrar/edificios');
        }
    }

    /** Fin Edificios */

    /** Salones*/

    public function ver_edificio($id_edificio)
    {

        //Restriccion
        vista_aplicaciones(4);

        //Validar que el id exista
        if (!empty($id_edificio)) {
            //Llamar datos del cliente
            $datos['edificio'] = $this->edificios->ver_edificio($id_edificio);

            if (!empty($datos['edificio']) && $datos['edificio'] != null) {

                //Datos vista
                $title['title'] = 'Ver edificio';
                $footer['mensaje'] = $this->mensajes();
                $datos['salones'] = $this->salones->listar_salones_edificio($id_edificio);

                //Vistas
                $this->load->view('templates/header',$title);
                $this->load->view('templates/menu');
                $this->load->view('administrar/salones/salones_listar',$datos);
                $this->load->view('templates/footer',$footer);

            } else {
                //No se encontro el programa
                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el edificio')));
                redirect('Administrar/edificios');
            }
        } else {
            //No se encontro el programa
            $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el edificio')));
            redirect('Administrar/edificios');
        }
    }

    public  function crear_salon($id_edificio){

        //Restriccion
        vista_aplicaciones(4);

        //Validar que el id exista
        if (!empty($id_edificio)) {
            //Llamar datos del cliente
            $datos['edificio'] = $this->edificios->ver_edificio($id_edificio);

            if (!empty($datos['edificio']) && $datos['edificio'] != null) {

                //Validar campos requeridos
                $this->form_validation->set_rules('codigo', 'Código', 'required');
                $this->form_validation->set_rules('nombre', 'Nombre', 'required');
                $this->form_validation->set_rules('estado', 'Estado', 'required');
                $this->form_validation->set_rules('capacidad', 'Capacidad', 'required');

                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Crear salón';
                    $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion',array(),true): '';

                    //Llamar la vista
                    $this->load->view('templates/header',$titulo);
                    $this->load->view('templates/menu');
                    $this->load->view('administrar/salones/salones_crear',$datos);
                    $this->load->view('templates/footer');
                } else {
                    //Organizar los datos para crear el programa
                    $envio['CODIGO'] = $this->input->post('codigo');
                    $envio['NOMBRE'] = $this->input->post('nombre');
                    $envio['ESTADO'] = $this->input->post('estado');
                    $envio['CAPACIDAD'] = $this->input->post('capacidad');
                    $envio['EDI_CODIGO'] = $id_edificio;

                    //Validar codigo
                    if ($this->salones->ver_salon($envio['CODIGO']) != null){
                        $titulo['titulo'] = 'Crear salón';
                        $titulo['mensaje'] = $this->load->view('templates/mensajes/mensaje_alerta',array('mensaje' => 'El código ya existe'),true);

                        //Llamar la vista
                        $this->load->view('templates/header',$titulo);
                        $this->load->view('templates/menu');
                        $this->load->view('administrar/salones/salones_crear',$datos);
                        $this->load->view('templates/footer');
                    }else{
                        //Crear salon
                        $id_programa = $this->salones->crear_salon($envio);
                        //Validar si se creo
                        if ($id_programa != false){
                            //Creación exitosa
                            $this->session->set_userdata(array('mensaje' => array('estado'=>1,'mensaje'=> 'Creación exitosa del salón '.$id_programa)));
                        }else{
                            //Creación fallida
                            $this->session->set_userdata(array('mensaje' => array('estado'=>4,'mensaje'=> 'No se pudo crear el salón ')));
                        }
                        redirect('Administrar/ver_edificio/'.$datos['edificio']['CODIGO']);
                    }
                }
            } else {
                //No se encontro el programa
                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el edificio')));
                redirect('Administrar/edificios');
            }
        } else {
            //No se encontro el programa
            $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el edificio')));
            redirect('Administrar/edificios');
        }
    }

    public  function modificar_salon($id_salon){

        //Restriccion
        vista_aplicaciones(4);

        //Validar que el id exista
        if (!empty($id_salon)) {
            //Llamar datos del cliente
            $datos['salon'] = $this->salones->ver_salon($id_salon);

            if (!empty($datos['salon']) && $datos['salon'] != null) {

                //Validar campos requeridos
                $this->form_validation->set_rules('codigo', 'Código', 'required');
                $this->form_validation->set_rules('nombre', 'Nombre', 'required');
                $this->form_validation->set_rules('capacidad', 'Capacidad', 'required');
                $this->form_validation->set_rules('estado', 'Estado', 'required');


                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Modificar salón';
                    $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion',array(),true): '';

                    //Llamar la vista
                    $this->load->view('templates/header',$titulo);
                    $this->load->view('templates/menu');
                    $this->load->view('administrar/salones/salones_modificar',$datos);
                    $this->load->view('templates/footer');
                } else {
                    //Organizar los datos para crear el programa
                    $envio['CODIGO'] = $this->input->post('codigo');
                    $envio['NOMBRE'] = $this->input->post('nombre');
                    $envio['ESTADO'] = $this->input->post('estado');
                    $envio['CAPACIDAD'] = $this->input->post('capacidad');

                    $codigo = $envio['CODIGO'];
                    $validar = $this->salones->ver_salon($envio['CODIGO']);

                    //Validar codigo
                    if ($validar != null && $codigo != $id_salon){
                        $titulo['titulo'] = 'Modificar salón';
                        $titulo['mensaje'] = $this->load->view('templates/mensajes/mensaje_alerta',array('mensaje' => 'El código ya existe'),true);

                        //Llamar la vista
                        $this->load->view('templates/header',$titulo);
                        $this->load->view('templates/menu');
                        $this->load->view('administrar/salones/salones_modificar',$datos);
                        $this->load->view('templates/footer');
                    }else{
                        //Modificar salon
                        $id_edificio_e = $this->salones->modificar_salon($id_salon, $envio);
                        //Validar si se modifico
                        if ($id_edificio_e != false){
                            //Creación exitosa
                            $this->session->set_userdata(array('mensaje' => array('estado'=>1,'mensaje'=> 'Modificación exitosa del salón '.$id_salon)));
                        }else{
                            //Creación fallida
                            $this->session->set_userdata(array('mensaje' => array('estado'=>4,'mensaje'=> 'No se pudo modificar el salón '.$id_salon)));
                        }
                        redirect('Administrar/ver_edificio/'.$datos['salon']['EDI_CODIGO']);
                    }
                }
            } else {
                //No se encontro el salon
                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el salón')));
                redirect('Administrar/edificios');
            }
        } else {
            //No se encontro el salón
            $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el salón')));
            redirect('Administrar/edificios');
        }
    }

    public function eliminar_salon($id_salon)
    {

        //Restriccion
        vista_aplicaciones(4);

        //Validar que el id exista
        if (!empty($id_salon)) {
            //Llamar datos del cliente
            $datos['salon'] = $this->salones->ver_salon($id_salon);

            if (!empty($datos['salon']) && $datos['salon'] != null) {

                //Validar campos requeridos
                $this->form_validation->set_rules('eliminar', 'Eliminar', 'required');

                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Eliminar salón';
                    $titulo['mensaje'] = (!empty(validation_errors())) ? $this->load->view('templates/mensajes/mensaje_validacion', array(), true) : '';

                    //Llamar la vista
                    $this->load->view('templates/header', $titulo);
                    $this->load->view('templates/menu');
                    $this->load->view('administrar/salones/salones_eliminar', $datos);
                    $this->load->view('templates/footer');
                } else {
                    //Organizar los datos para crear el cliente
                    $eliminar = $this->input->post('eliminar');
                    if ($eliminar === 'ELIMINAR'){

                        if ($this->horarios->validar_salon($id_salon) == null){
                            $id_salona_e = $this->salones->eliminar_salon($id_salon);

                            if ($id_salona_e != false){
                                //Se elimino correctamente
                                $this->session->set_userdata(array('mensaje' => array('estado' => 1, 'mensaje' => 'Eliminación exitosa del salón ' . $id_salon)));
                            }else{
                                //Eliminación fallida
                                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se pudo eliminar el salón '.$id_salon)));
                            }
                        }else{
                            //Si hay salones anclados al edificio no deja eliminar
                            $this->session->set_userdata(array('mensaje' => array('estado' => 2, 'mensaje' => 'No se pudo eliminar el salón ' . $id_salon . '<br>El edificio pertenece a uno o mas horarios')));
                        }
                    }else{
                        //Se escribio mal "ELIMINAR"
                        $this->session->set_userdata(array('mensaje' => array('estado' => 2, 'mensaje' => 'No se pudo eliminar el salón ' . $id_salon . '<br>Escribio mal "ELIMINAR"')));
                    }
                    redirect('Administrar/ver_edificio/'.$datos['salon']['EDI_CODIGO']);
                    //redirect('Inicio');
                }
            } else {
                //No se encontro el salon
                $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el salón')));
                redirect('Administrar/edificios');
            }
        } else {
            //No se encontro el salon
            $this->session->set_userdata(array('mensaje' => array('estado' => 4, 'mensaje' => 'No se encontro el esalóndificio')));
            redirect('Administrar/edificios');
        }
    }

    /** Fin Salones*/
}