<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 21/11/17
 * Time: 10:59 AM
 * Author: ideco.com.co
 */
class Login extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

    }
    /** Iniciar sesión*/
    public function index()
    {
        //Validar campos requeridos
        $this->form_validation->set_rules('documento', 'Documento', 'required');
        $this->form_validation->set_rules('clave', 'Clave', 'required');

        if ($this->form_validation->run() === FALSE) {
            //En caso que los campos se manden vascios
            if (!empty(validation_errors())){
                $datos['mensaje']=validation_errors();
                $this->load->view('Login/login',$datos);
            }else{
                $this->load->view('Login/login');
            }
        } else {

            //Obetener los valores de email y password
            $documento = $this->input->post('documento');
            $clave = md5($this->input->post('clave'));
            //Llamar los datos iguales de la base de datos
            $info_usuario = $this->usuarios->autenticacion($documento, $clave);
            //Validar si existe el usuario
            if($info_usuario === FALSE || $info_usuario === NULL){
                //Mensaje de datos incorrectos
                $datos['mensaje']="Usuario o Contraseña Incorrecto<br>Por favor Intentelo de nuevo.";
                $this->load->view('Login/login',$datos);
            } else {
                //Asigar como variable de session los datos importantes
                $this->session->set_userdata('id_usuario', $info_usuario['CODIGO']);
                $this->session->set_userdata('nombre', $info_usuario['NOMBRE']);
                $this->session->set_userdata('apellido', $info_usuario['APELLIDO']);
                $this->session->set_userdata('documento', $info_usuario['DOCUMENTO']);
                $this->session->set_userdata(array('mensaje' => array('estado'=>0,'mensaje'=> '')));

                //Aplicaciones
                $datos = $this->apli_usuarios->ver_apli_usuario($info_usuario['CODIGO']);
                if (!empty($datos)){
                    $this->session->set_userdata('aplicaciones', $datos);
                }else{
                    $this->session->set_userdata('aplicaciones', 0);
                }

                //Guiarlo al inicio
                redirect('');
            }
        }
    }
    /** Fin Iniciar sesión*/

    /** Cerrar sesión*/
    public function salir(){
        $this->session->sess_destroy();
        //Redireccionando a la página principal del usuario.
        redirect('Login', 'location', 301);
    }
    /** Fin Cerrar sesión*/

}