<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 14/05/2018
 * Time: 03:32 PM
 */
class Asignacion extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    /** Asignación manual */

    public function asignacion_manual()
    {
        //Restriccion
        vista_aplicaciones(5);

        //Datos vista
        $title['title'] = 'Asignación manual';
        $footer['mensaje'] = $this->mensajes();
        $datos['grupos'] = $this->grupos->listar_grupos();

        //Vistas
        $this->load->view('templates/header',$title);
        $this->load->view('templates/menu');
        $this->load->view('asignar/manual/asignacion_manual',$datos);
        $this->load->view('templates/footer',$footer);
    }

    /**
     *
     */
    public function buscar_salon()
    {
        if (!empty($_POST))
        {

            $data['grupo'] = $this->horarios->ver_horario($_POST['grupo'], fecha_editar($_POST['fecha']));

            if (!empty($data['grupo']))
            {
                $data['grupo']['grupo']= $_POST['grupo'];
                $data['grupo']['fecha']= $_POST['fecha'];

                $parametros = array(
                    'grupo'     => $_POST['grupo'],
                    'fecha'     => fecha_editar($_POST['fecha']),
                    'hora_ini'  => $data['grupo']['HORA_INICIO'],
                    'hora_fin'  => $data['grupo']['HORA_FIN'],
                    'salon'     => $data['grupo']['SAL_CODIGO'],
                    'capacidad' => $data['grupo']['CAPACIDAD']
                );

                $data['salones'] = $this->salones->listar_salones_disponibles($parametros);

                echo json_encode(array(
                    'status'    => true,
                    'mensaje'   => 'Busqueda Realizada',
                    'vista'     => $data
                ));
            }else{
                echo json_encode(array(
                    'status'    => false,
                    'mensaje'   => 'No se encontro el horario'
                ));
            }

        }else{
            echo json_encode(array(
                'status'    => false,
                'mensaje'   => 'No resivio nada'
            ));
        }

    }

    public function guardar_salon()
    {
            if (!empty($_POST))
            {
                $data['salon'] = $_POST['salon-g'];
                $data['grupo'] = $_POST['grupo-g'];
                $data['fecha'] = fecha_editar($_POST['fecha-g']);

                $this->horarios->editar_salon($data);


                echo json_encode(array(
                    'status'    => true,
                    'mensaje'   => 'Modificación Exitosa'
                ));
            }else{
                echo json_encode(array(
                    'status'    => false,
                    'mensaje'   => 'No se encontro el horario'
                ));
            }

    }

    /** Fin Asignación manual */

    /** Asignación automaticas*/

    public function asignacion_automatica()
    {
        //Restriccion
        vista_aplicaciones(6);

        //Datos vista
        $title['title'] = 'Asignación automatica';
        $footer['mensaje'] = $this->mensajes();
        $datos['programas'] = $this->programas->listar_programas();

        //Vistas
        $this->load->view('templates/header',$title);
        $this->load->view('templates/menu');
        $this->load->view('asignar/automatica/asignacion_automatica',$datos);
        $this->load->view('templates/footer',$footer);
    }

    public function guardar_asignacion_automatica()
    {
        if(!empty($_POST))
        {
            $limpiado = $this->horarios->limpiar_salon($_POST['programa']);

            if ($limpiado != false)
            {
                $grupos = $this->grupos->listar_grupos($_POST['programa']);

                if (!empty($grupos))
                {
                    $grupos_sin_salon = "";

                    foreach ($grupos as $grupo)
                    {
//                        $salon = $this->salones->lisar_salones_programa($grupo['CODIGO'], $grupo['CAPACIDAD']);

                        $hora = $this->horarios->hora_grupo($grupo['CODIGO']);
                        $salon = $this->salones->lisar_salones_programa(
                            array(
                                'grupo'     => $grupo['CODIGO'],
                                'hora_ini'  => $hora['HORA_INICIO'],
                                'hora_fin'  => $hora['HORA_FIN'],
                                'salon'     => $hora['SAL_CODIGO'],
                                'capacidad' => $grupo['CAPACIDAD']
                            )
                        );
                        //var_dump($salon);
                        if (!empty($salon)){
                            $update = $this->horarios->editar_salon_auto(array('grupo' => $grupo['CODIGO'], 'salon' => $salon['CODIGO']));
                            if ($update == false)
                            {
                                $grupos_sin_salon .= $grupo['CODIGO']."<br>";
                            }
                        }else{
                            $grupos_sin_salon .= $grupo['CODIGO']."<br>";
                        }
                    }

                    if ($grupos_sin_salon == "")
                    {
                        echo json_encode(array(
                            'status'    => 'success',
                            'title'     =>  'Hecho!',
                            'mensaje'   => 'Grupos asignados exitosamente'
                        ));
                    }else{
                        echo json_encode(array(
                            'status'    => 'warning',
                            'title'     =>  'Alerta!',
                            'mensaje'   => 'Grupos asignados exitosamente exepto<br>'.$grupos_sin_salon
                        ));
                    }
                }else{
                    echo json_encode(array(
                        'status'    => 'error',
                        'title'     =>  'Error!',
                        'mensaje'   => 'No se ncontraron grupos en el programa'
                    ));
                }
            }else{
                echo json_encode(array(
                    'status'    => 'error',
                    'title'     =>  'Error!',
                    'mensaje'   => 'No se limpio el programa'
                ));
            }

        }else{
            echo json_encode(array(
                'status'    => 'error',
                'title'     =>  'Error!',
                'mensaje'   => 'No se encontro el programa'
            ));
        }
    }



    /** Fin Asignación automaticas*/

    /** Calendario */
    public function calendario()
    {
        //Restriccion
        vista_aplicaciones(7);

        //Datos vista
        $title['title'] = 'Calendario';
        $footer['mensaje'] = $this->mensajes();
        $datos['programas'] = $this->programas->listar_programas();

        //Vistas
        $this->load->view('templates/header',$title);
        $this->load->view('templates/menu');
        $this->load->view('asignar/calendario/calendario',$datos);
        $this->load->view('templates/footer',$footer);
    }

    public function buscar_calendario()
    {
        if(!empty($_POST))
        {
            $programa = $this->horarios->calendario($_POST['programa']);
            $envio = array();
            if (!empty($programa))
            {
                //print_r($programa);
                foreach ($programa as $item)
                {
                    
                    if ($item['HORA_INICIO'] <= 9)
                    {
                        $item['HORA_INICIO'] = "0".$item['HORA_INICIO'];
                    }
                    if ($item['HORA_FIN'] <= 9)
                    {
                        $item['HORA_FIN'] = "0".$item['HORA_FIN'];
                    }
                    //print_r($item);
                    $envio[] = array(
                        'title' => $item['nombre_materia'].'-'.$item['codigo_grupo'].'-'.$item['SAL_CODIGO'],
                        'start' => $item['FECHA'].'T'.$item['HORA_INICIO'].':00:00',
                        'end' => $item['FECHA'].'T'.$item['HORA_FIN'].':00:00'
                    );

                }
            }else{
                $envio = false;
            }

            echo json_encode(array(
                'status'    => 'success',
                'title'     => 'Hecho!',
                'mensaje'   => 'Busqueda exitosa',
                'eventos'   => $envio
            ));

        }else{
            echo json_encode(array(
                'status'    => 'error',
                'title'     =>  'Error!',
                'mensaje'   => 'No se encontro el programa',
                'eventos'   => false
            ));
        }
    }
    /** Fin Calendario */
}