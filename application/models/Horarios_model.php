<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 01/05/2018
 * Time: 04:11 PM
 */
class Horarios_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //Validar si un edificio esta anclado a algun salon
    public function validar_salon($id_salon)
    {
        $this->db->select('horarios.*');
        $this->db->where('horarios.SAL_CODIGO', $id_salon);
        $this->db->limit(1);
        $query = $this->db->get('horarios');
        return $query->row_array();
    }

    //Validar si existe el horario pedido
    public function ver_horario($grupo, $fecha)
    {
        $this->db->select('horarios.*');
        $this->db->select('grupos.CAPACIDAD');

        $this->db->join('grupos','horarios.GRU_CODIGO = grupos.CODIGO','inner');

        $this->db->where('horarios.GRU_CODIGO', $grupo);
        $this->db->where('horarios.FECHA', $fecha);

        $query = $this->db->get('horarios');
        return $query->row_array();
    }

    public function hora_grupo($grupo)
    {
        $this->db->select('horarios.*');

        $this->db->where('horarios.GRU_CODIGO', $grupo);

        $this->db->group_by('horarios.GRU_CODIGO');

        $query = $this->db->get('horarios');
        return $query->row_array();
    }


    //Edita el salon
    public function editar_salon($data)
    {
        $this->db->where('horarios.FECHA', $data['fecha']);
        $this->db->where('horarios.GRU_CODIGO', $data['grupo']);
        $estado = false ;

        if (empty($data['salon']))
        {
            $data['salon'] = null;
        }

        $id = $this->db->update('horarios', array('SAL_CODIGO' => $data['salon']));

        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }

    }
    public function editar_salon_auto($data)
    {
        $this->db->where('horarios.GRU_CODIGO', $data['grupo']);

        if (empty($data['salon']))
        {
            $data['salon'] = null;
        }

        $id = $this->db->update('horarios', array('SAL_CODIGO' => $data['salon']));

        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }

    }

    public function limpiar_salon($programa)
    {
        $this->db->where('horarios.GRU_CODIGO in (select CODIGO from grupos where PL_CODIGO = '.$programa.')');
        $id = $this->db->update('horarios', array('SAL_CODIGO' => null));

        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }

    }


    public function calendario($programa)
    {
        $this->db->select('horarios.*');
        $this->db->select('grupos.CODIGO as codigo_grupo');
        $this->db->select('materias.NOMBRE as nombre_materia');

        $this->db->join('grupos','horarios.GRU_CODIGO = grupos.CODIGO','inner');
        $this->db->join('materias','grupos.COD_MATERIA = materias.CODIGO','inner');

        $this->db->where('grupos.PL_CODIGO', $programa);

        $query = $this->db->get('horarios');
        return $query->result_array();
    }



}