<?php
/**
 * Created by PhpStorm.
 * User: Kaneki Ken
 * Date: 14/05/2018
 * Time: 07:50 PM
 */
class Periodos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function listar_periodos()
    {
        $this->db->select('*');
        $query = $this->db->get('periodos');
        return $query->result_array();
    }
}