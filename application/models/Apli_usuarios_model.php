<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 22/04/2018
 * Time: 12:39 PM
 * Author: Cesar Antolinez
 */
class Apli_usuarios_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();

    }

    //Crear accesos a las aplicaciones
    public function crear_apli_usuario($envio)
    {
        $this->db->insert_batch('apl_usuarios', $envio);
        $id = $this->db->insert_id();
        if (!empty($id) && $id != null && $id != false ) {
            return $id;
        }else{
            return false;
        }
    }

    //elimnar accesos a las aplicaciones
    public function eliminar_apli_usuario($id_usuario)
    {
        $this->db->where('apl_usuarios.COD_USUARIO', $id_usuario);
        $id = $this->db->delete('apl_usuarios');
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }

    //Llamar las aplicaiones del usuario
    public function ver_apli_usuario($id_usuario)
    {
        $this->db->select('*');
        $this->db->where('apl_usuarios.COD_USUARIO', $id_usuario);
        $query = $this->db->get('apl_usuarios');
        return $query->result_array();
    }
}