<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 29/04/2018
 * Time: 05:39 PM
 */
class Grupos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //Validar si un programa esta anclado a algun grupo
    public function validar_grupo($id_programa)
    {
        $this->db->select('grupos.*');
        $this->db->where('grupos.PL_CODIGO', $id_programa);
        $this->db->limit(1);
        $query = $this->db->get('grupos');
        return $query->row_array();
    }

    //Llamar todos los grupos
    public function listar_grupos($programa = false)
    {
        $this->db->select('grupos.*');
        $this->db->select('materias.NOMBRE');
        $this->db->join('materias', 'materias.CODIGO = grupos.COD_MATERIA' , 'inner');
        if ($programa != false)
        {
            $this->db->where('grupos.PL_CODIGO', $programa);
        }
        $query = $this->db->get('grupos');
        return $query->result_array();
    }

}