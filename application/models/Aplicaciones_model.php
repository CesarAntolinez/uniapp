<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 22/04/2018
 * Time: 12:15 PM
 * Author: Cesar Antolinez
 */
class Aplicaciones_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();

    }

    //Llamar a todas las aplicaiones
    public function listar_aplicaciones()
    {
        $this->db->select('*');
        $query = $this->db->get('aplicaciones');
        return $query->result_array();
    }


}