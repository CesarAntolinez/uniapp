<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 29/04/2018
 * Time: 12:36 PM
 */
class Edificios_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    //Crear un edificio
    public function crear_edificio($envio)
    {
        $this->db->insert('edificios', $envio);
        $id = $envio['CODIGO'];
        if (!empty($id) && $id != null && $id != false ) {
            return $id;
        }else{
            return false;
        }
    }

    //ver todos los edificio
    public function listar_edificios()
    {
        $this->db->select('*');
        $query = $this->db->get('edificios');
        return $query->result_array();
    }

    //ver un edificio
    public function ver_edificio($id_edificio)
    {
        $this->db->select('*');
        $this->db->where('edificios.CODIGO', $id_edificio);
        $query = $this->db->get('edificios');
        return $query->row_array();
    }

    //modificar el edificio
    public function modificar_edificio($id_edificio,$envio)
    {
        $this->db->where('edificios.CODIGO', $id_edificio);
        $id = $this->db->update('edificios', $envio);
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }

    //Eliminar el edificio
    public function eliminar_edificio($id_edificio)
    {
        $this->db->where('edificios.CODIGO', $id_edificio);
        $id = $this->db->delete('edificios');
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }

}