<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 29/04/2018
 * Time: 12:31 PM
 */
class Programas_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //Crear un programa
    public function crear_programa($envio)
    {
        $this->db->insert('programas', $envio);
        $id = $envio['CODIGO'];
        if (!empty($id) && $id != null && $id != false ) {
            return $id;
        }else{
            return false;
        }
    }

    //ver todos los programa
    public function listar_programas()
    {
        $this->db->select('*');
        $query = $this->db->get('programas');
        return $query->result_array();
    }

    //ver un programa
    public function ver_programa($id_programa)
    {
        $this->db->select('*');
        $this->db->where('programas.CODIGO', $id_programa);
        $query = $this->db->get('programas');
        return $query->row_array();
    }

    //modificar el programa
    public function modificar_programa($id_programa,$envio)
    {
        $this->db->where('programas.CODIGO', $id_programa);
        $id = $this->db->update('programas', $envio);
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }

    //Eliminar el programa
    public function eliminar_programa($id_programa)
    {
        $this->db->where('programas.CODIGO', $id_programa);
        $id = $this->db->delete('programas');
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }


}