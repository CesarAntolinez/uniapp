<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 29/04/2018
 * Time: 12:35 PM
 */
class Salones_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //Validar si un edificio esta anclado a algun salon
    public function validar_edificio($id_edificio)
    {
        $this->db->select('salones.*');
        $this->db->where('salones.EDI_CODIGO', $id_edificio);
        $this->db->limit(1);
        $query = $this->db->get('salones');
        return $query->row_array();
    }

    //Ver los salones de un edificio
    public function listar_salones_edificio($id_edificio)
    {
        $this->db->select('salones.*');
        $this->db->where('salones.EDI_CODIGO', $id_edificio);
        $query = $this->db->get('salones');
        return $query->result_array();
    }

    //modificar el salon
    public function modificar_salon($id_salon,$envio)
    {
        $this->db->where('salones.CODIGO', $id_salon);
        $id = $this->db->update('salones', $envio);
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }

    //Eliminar el salon
    public function eliminar_salon($id_salon)
    {
        $this->db->where('salones.CODIGO', $id_salon);
        $id = $this->db->delete('salones');
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }

    //Crear un salon
    public function crear_salon($envio)
    {
        $this->db->insert('salones', $envio);
        $id = $envio['CODIGO'];
        if (!empty($id) && $id != null && $id != false ) {
            return $id;
        }else{
            return false;
        }
    }

    //ver un edificio
    public function ver_salon($id_salon)
    {
        $this->db->select('*');
        $this->db->where('salones.CODIGO', $id_salon);
        $query = $this->db->get('salones');
        return $query->row_array();
    }

    //Validar los salones disponibles
    public function listar_salones_disponibles($parametros)
    {
        $parametros['hora_fin'] = $parametros['hora_fin']-0.1;

        $query = $this->db->query('select  salones.* from salones 
                    where salones.CAPACIDAD >= '.$parametros['capacidad'].' 
                    and salones.CODIGO not in (select horarios.SAL_CODIGO  from horarios 
                    where horarios.FECHA ='.$parametros['fecha'].' and horarios.SAL_CODIGO is not null  and  
                    ((horarios.HORA_INICIO between '. $parametros['hora_ini'] .' and '. $parametros['hora_fin'] .') 
                    or (horarios.HORA_FIN between '. $parametros['hora_ini'] .' and '. $parametros['hora_fin'] .') 
                    or ('. $parametros['hora_ini'] .' between  horarios.HORA_INICIO and horarios.HORA_FIN) 
                    or ('. $parametros['hora_fin'] .' between  horarios.HORA_INICIO and horarios.HORA_FIN)) and horarios.SAL_CODIGO != \''.$parametros['salon'].'\' 
                    group by horarios.SAL_CODIGO)');

        //$query = $this->db->get();
        return $query->result_array();
    }

    //Validar los salones disponibles
    public function lisar_salones_programa($parametros)
    {
        $query = $this->db->query('SELECT 
    salones.*
FROM
    salones
WHERE
    salones.CAPACIDAD >= '.$parametros['capacidad'].'
        AND salones.CODIGO NOT IN (SELECT 
            horarios.SAL_CODIGO
        FROM
            horarios
        WHERE
            horarios.FECHA IN (SELECT FECHA FROM horarios WHERE GRU_CODIGO = '.$parametros['grupo'].')
                AND horarios.SAL_CODIGO IS NOT NULL
                AND ((horarios.HORA_INICIO BETWEEN '.$parametros['hora_ini'].' AND '.$parametros['hora_fin'].')
                OR (horarios.HORA_FIN BETWEEN '.$parametros['hora_ini'].' AND '.$parametros['hora_fin'].')
                OR ('.$parametros['hora_ini'].' BETWEEN horarios.HORA_INICIO AND horarios.HORA_FIN)
                OR ('.$parametros['hora_fin'].' BETWEEN horarios.HORA_INICIO AND horarios.HORA_FIN))
        GROUP BY horarios.SAL_CODIGO)
        order by CAPACIDAD ASC
        limit 1;');
        return $query->row_array();
    }


}