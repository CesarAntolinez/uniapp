<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 22/04/2018
 * Time: 12:34 PM
 * Author: Cesar Antolinez
 */
class Usuarios_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    //Crear un usuario
    public function crear_usuario($envio)
    {
        $this->db->insert('usuarios', $envio);
        $id = $this->db->insert_id();
        if (!empty($id) && $id != null && $id != false ) {
            return $id;
        }else{
            return false;
        }
    }

    //ver el usuario
    public function listar_usuarios()
    {
        $this->db->select('*');
        $query = $this->db->get('usuarios');
        return $query->result_array();
    }

    //ver el usuario
    public function ver_usuario($id_usuario)
    {
        $this->db->select('*');
        $this->db->where('usuarios.CODIGO', $id_usuario);
        $query = $this->db->get('usuarios');
        return $query->row_array();
    }

    //modificar el usuario
    public function modificar_usuario($id_usuario,$envio)
    {
        $this->db->where('usuarios.CODIGO', $id_usuario);
        $id = $this->db->update('usuarios', $envio);
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }

    //Eliminar el usuario
    public function eliminar_usuario($id_usuario)
    {
        $this->db->where('usuarios.CODIGO', $id_usuario);
        $id = $this->db->delete('usuarios');
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }

    //Función que Autentica a un usuario
    public function autenticacion($correo, $clave)
    {
        if (($correo === FALSE) or ($clave === FALSE)) {
            return FALSE;
        } else {
            $this->db->select('*');
            $this->db->where('DOCUMENTO', $correo);
            $this->db->where('CLAVE', $clave);
            $query = $this->db->get('usuarios');
            return $query->row_array();
        }
    }
}