<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 22/04/2018
 * Time: 01:29 PM
 * Author: Cesar Antolinez
 */
function buscar_aplicacion( $buscar , $arreglo = array() ){
    $estado = false;
    //var_dump($arreglo);
    if (!empty($buscar) && is_array($arreglo)){
        foreach ($arreglo as $item){

            if ($buscar == $item['COD_APLICACION']){
                //echo $buscar.'=='.$item['COD_APLICACION'].' '.'<br>';
                $estado = true;
                //break;
            }
        }
    }
    return $estado;
}

//permiso de vista
function vista_aplicaciones($id_aplicaion){
    //$aplicaciones = $this->session->userdata('aplicaciones');
    $aplicaciones = $_SESSION['aplicaciones'];
    $estado = false;
    if ($aplicaciones != null){
        foreach ($aplicaciones as $item){
            if ($item['COD_APLICACION'] == $id_aplicaion){
                $estado = true;
            }
        }
    }
    if ($estado == false){
        redirect('Inicio');
    }
}

//permiso de vista
function elemntos_aplicaciones($id_aplicaion){
    $aplicaciones = $_SESSION['aplicaciones'];
    $estado = false;
    if ($aplicaciones != null){
        foreach ($aplicaciones as $item){
            if ($item['COD_APLICACION'] == $id_aplicaion){
                $estado = true;
            }
        }
    }
    return $estado;
}

function is_same($var1, $var2, $stric = false)
{
    if ($stric == true) {
        if ($var1 === $var2) {
            return true;
        } else {
            return false;
        }
    } else {

        if ($var1 == $var2) {
            return true;
        } else {
            return false;
        }
    }
}

function aplicaciones($id_aplicaion){
    //$aplicaciones = $this->session->userdata('aplicaciones');
    $aplicaciones = $_SESSION['aplicaciones'];
    $estado = false;
    if ($aplicaciones != null){
        foreach ($aplicaciones as $item){
            if ($item['COD_APLICACION'] == $id_aplicaion){
                $estado = true;
            }
        }
    }
    return $estado;
}

function retornar_nombre_apellido(){
    //$aplicaciones = $this->session->userdata('aplicaciones');
    $nombre = $_SESSION['nombre'];
    $apellido = $_SESSION['apellido'];
    return $nombre.' '.$apellido;
}
function retornar_nombre(){
    return $_SESSION['nombre'];
}

function fecha_editar($fecha){
    return date('Y-m-d',strtotime($fecha));
}

function fecha_ver($fecha){
    return date('m/d/Y',strtotime($fecha));
}
