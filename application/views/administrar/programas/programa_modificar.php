<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 29/04/2018
 * Time: 01:07 PM
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Programas</h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Administrar/programas') ?>"><i class="fa fa-dashboard"></i> Programas</a></li>
            <li class="active">Modificar Programa</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Modificar Programa</h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>-->
                </div>
            </div>
            <form action="<?= site_url('Administrar/modificar_programa/'.$programa['CODIGO']) ?>" class="form" method="post" id="programas-form-modificar">
                <div class="box-body">
                    <div class="col-md-offset-1 col-md-10">
                        <div class="col-md-6 form-group">
                            <label for="codigo">Código:</label>
                            <input type="number" id="codigo" name="codigo" class="form-control" value="<?= set_value('codigo',$programa['CODIGO']) ?>" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="nombre">Nombre:</label>
                            <input type="text" id="nombre" name="nombre" class="form-control" value="<?= set_value('nombre',$programa['NOMBRE']) ?>" required>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-offset-1 col-md-10">
                        <a href="<?= site_url('Administrar/programas') ?>" class="btn btn-default pull-left"><i class="fa fa-arrow-left"></i> Regresar</a>
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-edit"></i> Modificar Programa</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>