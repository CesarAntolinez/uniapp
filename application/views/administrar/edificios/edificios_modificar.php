<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 29/04/2018
 * Time: 06:50 PM
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Edificios</h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Administrar/edificios') ?>"><i class="fa fa-dashboard"></i> Edificios</a></li>
            <li class="active">Modificar Edificio</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Modificar Edificio</h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>-->
                </div>
            </div>
            <form action="<?= site_url('Administrar/modificar_edificio/'.$edificio['CODIGO']) ?>" class="form" method="post" id="edificios-form-modificar">
                <div class="box-body">
                    <div class="col-md-offset-1 col-md-10">
                        <div class="col-md-6 form-group">
                            <label for="codigo">Código:</label>
                            <input type="number" id="codigo" name="codigo" class="form-control" value="<?= set_value('codigo', $edificio['CODIGO']) ?>" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="nombre">Nombre:</label>
                            <input type="text" id="nombre" name="nombre" class="form-control" value="<?= set_value('nombre', $edificio['NOMBRE']) ?>" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="estado">Estado:</label>
                            <select name="estado" id="estado" class="form-control">
                                <option></option>
                                <option value="0" <?= set_select('estado',0,is_same(0,$edificio['ESTADO'])) ?> >No disponible</option>
                                <option value="1" <?= set_select('estado',1,is_same(1,$edificio['ESTADO'])) ?> >Disponible</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-offset-1 col-md-10">
                        <a href="<?= site_url('Administrar/edificios') ?>" class="btn btn-default pull-left"><i class="fa fa-arrow-left"></i> Regresar</a>
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Modificar Edificio</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>