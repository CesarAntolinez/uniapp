<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 29/04/2018
 * Time: 07:32 PM
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Edificios</h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Administrar/edificios') ?>"><i class="fa fa-dashboard"></i> Edificios</a></li>
            <li class="active">Eliminar Edificio</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Eliminar Edificio</h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>-->
                </div>
            </div>
            <form action="<?= site_url('Administrar/eliminar_edificio/'.$edificio['CODIGO']) ?>" class="form" method="post" id="edificio-form-eliminar">
                <div class="box-body">
                    <div class="col-md-offset-1 col-md-10">
                        <div class="col-md-12">
                            <span><b>Código :</b> <?= $edificio['CODIGO'] ?></span><br>
                            <span><b>Nombre :</b> <?= $edificio['NOMBRE'] ?></span><br>
                            <span><b>Estado :</b> <?= ($edificio['ESTADO'] == 1) ? 'Disponible': 'No disponible' ?></span>
                        </div>
                        <p></p>
                        <div class="col-md-12 form-group">
                            <label for="eliminar">Eliminar:</label>
                            <input type="text" id="eliminar" name="eliminar" class="form-control" value="<?= set_value('eliminar') ?>" required>
                            <span>Para poder eliminar este edificio debe escribir "ELIMINAR" sin las comillas y en mayuscula</span>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-offset-1 col-md-10">
                        <a href="<?= site_url('Administrar/edificios') ?>" class="btn btn-default pull-left"><i class="fa fa-arrow-left"></i> Regresar</a>
                        <button type="submit" class="btn btn-danger pull-right"><i class="fa fa-trash"></i> Eliminar Edificio</button>
                    </div>
                </div>
            </form>
        </div>
    </section>

</div>