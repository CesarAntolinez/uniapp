<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 01/05/2018
 * Time: 03:13 PM
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Salones</h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Administrar/edificios') ?>"><i class="fa fa-dashboard"></i> Edificios</a></li>
            <li class="active">Crear Salón</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Crear Salón</h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>-->
                </div>
            </div>
            <form action="<?= site_url('Administrar/crear_salon/'.$edificio['CODIGO']) ?>" class="form" method="post" id="salones-form-crear">
                <div class="box-body">
                    <div class="col-md-offset-1 col-md-10">
                        <div class="col-md-6 form-group">
                            <label for="codigo">Código:</label>
                            <input type="text" id="codigo" name="codigo" class="form-control" value="<?= set_value('codigo') ?>" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="nombre">Nombre:</label>
                            <input type="text" id="nombre" name="nombre" class="form-control" value="<?= set_value('nombre') ?>" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="capacidad">Capacidad:</label>
                            <input type="text" id="capacidad" name="capacidad" class="form-control" value="<?= set_value('capacidad') ?>" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="estado">Estado:</label>
                            <select name="estado" id="estado" class="form-control">
                                <option></option>
                                <option value="0" <?= set_select('estado',0) ?> >No disponible</option>
                                <option value="1" <?= set_select('estado',1) ?> >Disponible</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-offset-1 col-md-10">
                        <a href="<?= site_url('Administrar/ver_edificio/'.$edificio['CODIGO']) ?>" class="btn btn-default pull-left"><i class="fa fa-arrow-left"></i> Regresar</a>
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Crear Salón</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>