<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 01/05/2018
 * Time: 02:35 PM
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Salones</h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Administrar/edificios') ?>"><i class="fa fa-dashboard"></i> Edificios</a></li>
            <li class="active">Ver Edificio</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Ver Edificio</h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>-->
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-offset-1 col-md-10">
                    <div class="table-responsive">
                        <h3 class="text-center">Datos edificio</h3>
                        <table id="edificios-table-ver" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Datos</th>
                                <th>Información</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Código</td>
                                <td><?= $edificio['CODIGO'] ?></td>
                            </tr>
                            <tr>
                                <td>Nombre</td>
                                <td><?= $edificio['NOMBRE'] ?></td>
                            </tr>
                            <tr>
                                <td>Estado</td>
                                <td><?= ($edificio['ESTADO'] == 1) ? 'Disponible': 'No disponible' ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <h3 class="text-center">Salones</h3>
                        <table class="table table-bordered datatable" id="salones-table-listar">
                            <thead>
                            <tr>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Capacidad</th>
                                <th>Estado</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($salones)){
                                foreach ($salones as $item){
                                    ?>
                                    <tr>
                                        <td><?= $item['CODIGO'] ?></td>
                                        <td><?= $item['NOMBRE'] ?></td>
                                        <td><?= $item['CAPACIDAD'] ?></td>
                                        <td><?= ($item['ESTADO'] == 1) ? 'Disponible': 'No disponible' ?></td>
                                        <td>
                                            <a href="<?= site_url('Administrar/modificar_salon/'.$item['CODIGO']) ?>" class="btn btn-primary"><i class="fa fa-edit"></i> Modificar</a>
                                            <a href="<?= site_url('Administrar/eliminar_salon/'.$item['CODIGO']) ?>" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1 col-md-10">
                    <a href="<?= site_url('Administrar/edificios') ?>" class="btn btn-default pull-left"><i class="fa fa-arrow-left"></i> Regresar</a>
                    <a href="<?= site_url('Administrar/crear_salon/'.$edificio['CODIGO']) ?>" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Crear salón</a>
                </div>
            </div>
        </div>
    </section>
</div>