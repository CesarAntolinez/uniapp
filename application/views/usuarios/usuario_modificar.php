<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 22/04/2018
 * Time: 01:51 PM
 * Author: Cesar Antolinez
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Usuarios</h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Usuarios') ?>"><i class="fa fa-dashboard"></i> Usuarios</a></li>
            <li class="active">Modificar Usuario</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Modificar Usuario</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <form action="<?= site_url('Usuarios/modificar/'.$usuario['CODIGO']) ?>" class="form" method="post" id="usuarios-form-modificar">
                <div class="box-body">
                    <div class="col-md-offset-1 col-md-10">
                        <div class="col-md-6 form-group">
                            <label for="nombre">Nombre:</label>
                            <input type="text" id="nombre" name="nombre" class="form-control" value="<?= set_value('nombre',$usuario['NOMBRE']) ?>" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="apellido">Apellido:</label>
                            <input type="text" id="apellido" name="apellido" class="form-control" value="<?= set_value('apellido',$usuario['APELLIDO']) ?>" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="documento">Documento:</label>
                            <input type="number" id="documento" name="documento" class="form-control" value="<?= set_value('documento',$usuario['DOCUMENTO']) ?>" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="clave">Clave:</label>
                            <input type="text" id="calve" name="clave" class="form-control" value="<?= set_value('clave') ?>" >
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Aplicaciones:</label>
                            <div class="checkbox">
                                <?php
                                if (!empty($aplicaiones)){
                                    foreach ($aplicaiones as $item) {
                                        ?>
                                        <label class="aplicaiones-div-checkbox">
                                            <input class="aplicaciones-checkbox" type="checkbox" id="aplicaciones" name="aplicaciones[]" <?= set_checkbox('aplicaciones[]', $item['CODIGO'], buscar_aplicacion( $item['CODIGO'],$apli_usuario)) ?> value="<?= $item['CODIGO'] ?>"> <?= $item['NOMBRE'] ?>
                                        </label>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-offset-1 col-md-10">
                        <a href="<?= site_url('Usuarios') ?>" class="btn btn-default pull-left"><i class="fa fa-arrow-left"></i> Regresar</a>
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-edit"></i> Modificar Usuario</button>
                    </div>
                </div>
            </form>
        </div>
    </section>

</div>