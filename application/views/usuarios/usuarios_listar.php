<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 22/04/2018
 * Time: 04:10 PM
 * Author: Cesar Antolinez
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Usuarios</h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-dashboard"></i> Usuarios</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Usuarios</h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>-->
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-offset-1 col-md-10">
                    <div class="table-responsive">
                        <table class="table table-bordered datatable" id="usuario-table-listar">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Documento</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($usuarios)){
                                foreach ($usuarios as $item){
                                    ?>
                                    <tr>
                                        <td><?= $item['CODIGO'] ?></td>
                                        <td><?= $item['NOMBRE'] ?></td>
                                        <td><?= $item['APELLIDO'] ?></td>
                                        <td><?= $item['DOCUMENTO'] ?></td>
                                        <td>
                                            <a href="<?= site_url('Usuarios/modificar/'.$item['CODIGO']) ?>" class="btn btn-primary"><i class="fa fa-edit"></i> Modificar</a>
                                            <a href="<?= site_url('Usuarios/eliminar/'.$item['CODIGO']) ?>" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1 col-md-10">
                    <a href="<?= site_url('Usuarios/crear') ?>" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Crear Usuario</a>
                </div>
            </div>
        </div>
    </section>
</div>