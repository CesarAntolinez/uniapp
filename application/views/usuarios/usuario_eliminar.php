<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 22/04/2018
 * Time: 03:59 PM
 * Author: Cesar Antolinez
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Usuarios</h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Usuarios') ?>"><i class="fa fa-dashboard"></i> Usuarios</a></li>
            <li class="active">Eliminar Usuario</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Eliminar Usuario</h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>-->
                </div>
            </div>
            <form action="<?= site_url('Usuarios/eliminar/'.$usuario['CODIGO']) ?>" class="form" method="post" id="usuarios-form-eliminar">
                <div class="box-body">
                    <div class="col-md-offset-1 col-md-10">
                        <div class="col-md-12">
                            <span><b>Nombre :</b> <?= $usuario['NOMBRE'] ?></span><br>
                            <span><b>Apellido :</b> <?= $usuario['APELLIDO'] ?></span>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="eliminar">Eliminar:</label>
                            <input type="text" id="eliminar" name="eliminar" class="form-control" value="<?= set_value('eliminar') ?>" required>
                            <span>Para poder eliminar este usuario debe escribir "ELIMINAR" sin las comillas y en mayuscula</span>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-offset-1 col-md-10">
                        <a href="<?= site_url('Usuarios') ?>" class="btn btn-default pull-left"><i class="fa fa-arrow-left"></i> Regresar</a>
                        <button type="submit" class="btn btn-danger pull-right"><i class="fa fa-trash"></i> Eliminar Usuario</button>
                    </div>
                </div>
            </form>
        </div>
    </section>

</div>