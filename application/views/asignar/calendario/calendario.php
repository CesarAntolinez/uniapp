<?php
/**
 * Created by PhpStorm.
 * User: cesar antolinez
 * Date: 30/05/18
 * Time: 03:08 PM
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Asignación</h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-dashboard"></i> Asignación automatica</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Asignación automatica</h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>-->
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-offset-1 col-md-10">
                    <form action="<?= site_url('Asignacion/buscar_calendario') ?>" class="form" method="post" id="asignacion-calendario-form">
                        <div class="col-xs-12 col-md-10 form-group">
                            <label for="grupo">Programas:</label>
                            <select name="programa" id="programa" class="form-control select2" required>
                                <option></option>
                                <?php
                                foreach ($programas as $item){
                                    echo '<option value="'.$item['CODIGO'].'"> '. $item['CODIGO'] . '-' . $item['NOMBRE'] .' </option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-xs-3 col-md-2 form-group">
                            <br>
                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Buscar</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-offset-1 col-md-10">
                    <div id='calendar'></div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1 col-md-10">
                    <a href="<?= site_url() ?>" class="btn btn-default pull-left"><i class="fa fa-arrow-left"></i> Regresar</a>
                </div>
            </div>

        </div>
    </section>
</div>
