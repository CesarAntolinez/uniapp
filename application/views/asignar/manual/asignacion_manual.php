<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 14/05/2018
 * Time: 05:00 PM
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Asignación</h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-dashboard"></i> Asignación manual</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Asignación manual</h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>-->
                </div>
            </div>
            <form action="<?= site_url('Asignacion/buscar_salon') ?>" class="form" method="post" id="asignacion-manual-buscar-salon-form">
                <div class="box-body">
                    <div class="col-md-offset-1 col-md-10">
                        <div class="col-xs-12 col-md-5 form-group">
                            <label for="grupo">Grupos:</label>
                            <select name="grupo" id="grupo" class="form-control select2">
                                <option></option>
                                <?php
                                foreach ($grupos as $item){
                                    echo '<option value="'.$item['CODIGO'].'"> '. $item['CODIGO'] . '-' . $item['NOMBRE'] .' </option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-xs-9 col-md-5 form-group">
                            <label for="fecha">Fecha:</label>
                            <input type="text" name="fecha" id="fecha" class="fecha form-control">
                        </div>
                        <div class="col-xs-3 col-md-2 form-group">
                            <br>
                            <button type="button" id="buscar-salones" class="btn btn-success"><i class="fa fa-search"></i> Buscar</button>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-offset-1 col-md-10">
                        <a href="<?= site_url() ?>" class="btn btn-default pull-left"><i class="fa fa-arrow-left"></i> Regresar</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
<div class="modal" id="asignacion-manual-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Asignar salon</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height: 120px !important;">
                <form action="<?= site_url('Asignacion/guardar_salon') ?>" id="asignacion-manual-guardar-salon-form">
                    <div class="row">
                        <input type="hidden" name="fecha-g" id="fecha-g">
                        <input type="hidden" name="grupo-g" id="grupo-g">
                        <div class="col-xs-6"><b>Grupo: </b><small id="grupo-t"></small> </div>
                        <div class="col-xs-6"><b>Fecha: </b><small id="fecha-t"></small> </div>
                        <p></p>
                        <div class="col-xs-12 form-group">
                            <label for="salon-g">Salón</label>
                            <select name="salon-g" id="salon-g" class="form-control">
                                <option></option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                <button type="button" class="btn btn-primary" id="guardar-salones"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>
