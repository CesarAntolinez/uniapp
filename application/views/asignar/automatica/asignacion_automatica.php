<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 29/05/2018
 * Time: 11:06 PM
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Asignación</h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-dashboard"></i> Asignación automatica</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Asignación automatica</h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>-->
                </div>
            </div>
            <form action="<?= site_url('Asignacion/guardar_asignacion_automatica') ?>" class="form" method="post" id="asignacion-automatica-form">
                <div class="box-body">
                    <div class="col-md-offset-1 col-md-10">
                        <div class="col-xs-12 col-md-10 form-group">
                            <label for="grupo">Programas:</label>
                            <select name="programa" id="programa" class="form-control select2" required>
                                <option></option>
                                <?php
                                foreach ($programas as $item){
                                    echo '<option value="'.$item['CODIGO'].'"> '. $item['CODIGO'] . '-' . $item['NOMBRE'] .' </option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-xs-3 col-md-2 form-group">
                            <br>
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Asignar</button>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-offset-1 col-md-10">
                        <a href="<?= site_url() ?>" class="btn btn-default pull-left"><i class="fa fa-arrow-left"></i> Regresar</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

