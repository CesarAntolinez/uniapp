<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 14/03/18
 * Time: 5:48 PM
 * Author: Cesar Antolinez
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Alerta -->
<script type="text/javascript">
    swal({
        type: 'success',
        title: 'Hecho!',
        html: '<?= $mensaje?>'
    })
</script>