<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 14/03/18
 * Time: 5:50 PM
 * Author: Cesar Antolinez
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="hidden" id="form">
    <?= validation_errors()?>
</div>
<!-- Alerta -->
<script type="text/javascript">
    swal({
        type: 'question',
        title: 'Alerta!',
        html: $('#form').html()
    })
</script>
