<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 14/03/18
 * Time: 5:45 PM
 * Author: Cesar Antolinez
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Alerta -->
<script type="text/javascript">
    swal({
        type: 'error',
        title: 'Error!',
        html: '<?= $mensaje?>'
    })
</script>
