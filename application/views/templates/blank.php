<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 6/04/18
 * Time: 11:18 AM
 * Author: Cesar Antolinez
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Inicio<small> Optional description</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Title</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                Start creating your amazing application!
            </div>
            <div class="box-footer">
                Footer
            </div>
        </div>
    </section>

</div>
