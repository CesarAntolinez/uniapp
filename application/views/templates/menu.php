<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 6/04/18
 * Time: 11:14 AM
 * Author: Cesar Antolinez
 */
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="<?= site_url() ?>"><i class="fa fa-link"></i> <span>Inicio</span></a></li>
            <?= (elemntos_aplicaciones(2)) ? '<li><a href="'.site_url('Usuarios').'"><i class="fa fa-link"></i> <span>Usuarios</span></a></li>':''?>
            <?php
            if (elemntos_aplicaciones(3) || elemntos_aplicaciones(4)){
                ?>
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i>
                        <span>Adiministrar</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <?= (elemntos_aplicaciones(3)) ? '<li><a href="'.site_url('Administrar/programas').'"><i class="fa fa-link"></i> <span>Programas</span></a></li>':''?>
                        <?= (elemntos_aplicaciones(4)) ? '<li><a href="'.site_url('Administrar/edificios').'"><i class="fa fa-link"></i> <span>Edificios</span></a></li>':''?>
                    </ul>
                </li>
                <?php
            }
            ?>

            <?php
            if (elemntos_aplicaciones(5) ){
                ?>
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i>
                        <span>Asignación</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <?= (elemntos_aplicaciones(5)) ? '<li><a href="'.site_url('Asignacion/asignacion_manual').'"><i class="fa fa-link"></i> <span>Asignación manual</span></a></li>':''?>
                        <?= (elemntos_aplicaciones(6)) ? '<li><a href="'.site_url('Asignacion/asignacion_automatica').'"><i class="fa fa-link"></i> <span>Asignación automatica</span></a></li>':''?>
                        <?= (elemntos_aplicaciones(7)) ? '<li><a href="'.site_url('Asignacion/calendario').'"><i class="fa fa-link"></i> <span>Calendario</span></a></li>':''?>
                    </ul>
                </li>
                <?php
            }
            ?>
        </ul>
    </section>
</aside>