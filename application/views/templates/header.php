<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 6/04/18
 * Time: 11:10 AM
 * Author: Cesar Antolinez
 */
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>UniApp <?= (isset($titulo)) ? '| '.$titulo: '' ?><?= (isset($title)) ? '| '.$title: '' ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url('public/plugins/bootstrap/dist/css/bootstrap.min.css') ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('public/plugins/font-awesome/css/font-awesome.min.css') ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url('public/plugins/Ionicons/css/ionicons.min.css') ?>">
    <!-- sweetalert2 -->
    <link rel="stylesheet" href="<?php echo base_url('public/plugins/sweetalert2/dist/sweetalert2.min.css')?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url('public/plugins/iCheck/square/orange.css')?>">
    <!-- DateTable -->
    <link rel="stylesheet" href="<?= base_url('public/plugins/DataTables/datatables.min.css')?>">
    <!-- Pace-master -->
    <link rel="stylesheet" href="<?php echo base_url('public/plugins/PACE/themes/orange/pace-theme-barber-shop.css')?>">
    <!-- datepicker -->
    <link rel="stylesheet" href="<?= base_url('public/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')?>">
    <!-- Select2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!-- fullcalendar -->
    <link href="<?= base_url('public/plugins/fullcalendar/dist/fullcalendar.min.css') ?>" rel="stylesheet" />

    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('public/css/AdminLTE.min.css') ?>">
    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?= base_url('public/css/skin-yellow-light.min.css') ?>">
    <!-- Style UniApp. -->
    <link rel="stylesheet" href="<?= base_url('public/css/style.css') ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-yellow-light sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="<?= site_url() ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>U</b>A</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Uni</b>App</span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?= base_url('public/images/avatar.png') ?>" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?= retornar_nombre(); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="<?= base_url('public/images/avatar.png') ?>" class="img-circle" alt="User Image">

                                <p>
                                    <?= retornar_nombre_apellido(); ?>

                                </p>
                            </li>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <!--<a href="#" class="btn btn-default btn-flat">Perfil</a>-->
                                </div>
                                <div class="pull-right">
                                    <a href="<?= site_url('Login/salir') ?>" class="btn btn-default btn-flat">Cerrar sesión</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>