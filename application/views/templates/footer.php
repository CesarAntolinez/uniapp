<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 6/04/18
 * Time: 11:05 AM
 * Author: Cesar Antolinez
 */
?>
<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright &copy; 2016 <a href="#">Cesar Antolinez</a>.</strong> Derechos reservados.
</footer>

<!-- jQuery 3 -->
<script src="<?= base_url('public/plugins/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('public/plugins/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('public/js/adminlte.min.js') ?>"></script>
<!-- sweetalert -->
<script src="<?= base_url('public/plugins/sweetalert2/dist/sweetalert2.min.js')?>"></script>
<!-- iCheck -->
<script src="<?= base_url('public/plugins/iCheck/icheck.min.js')?>"></script>
<!-- DataTables -->
<script src="<?= base_url('public/plugins/DataTables/datatables.min.js')?>"></script>
<!-- Pace -->
<script src="<?= base_url('public/plugins/PACE/pace.min.js')?>"></script>
<!-- datepicker -->
<script src="<?= base_url('public/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<!-- moment -->
<script src="<?= base_url('public/plugins/moment/min/moment.min.js')?>"></script>
<!-- fullcalendar -->
<script src="<?= base_url('public/plugins/fullcalendar/dist/fullcalendar.min.js')?>"></script>
<!-- Select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<!-- Script UniApp -->
<script src="<?= base_url('public/js/script.js')?>"></script>

<?= (isset($mensaje))? $mensaje : '' ?>

</body>
</html>