<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 11/04/2018
 * Time: 12:08 AM
 * Author: Cesar Antolinez
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Inicio</h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-dashboard"></i> Inicio</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Modelo de datos</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <img src="<?= base_url('public/images/DB.png') ?>" alt="DB" class="img-responsive">
            </div>
        </div>
    </section>

</div>

